<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shappy</title>

    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
    
<!-- header section starts  -->

<header class="header">

    <a href="#" class="logo"> shappy </a>

    <nav class="navbar">
        <a href="#home">home</a>
        <a href="#features">features</a>
        <a href="#products">products</a>
        <a href="#order">order</a>
    </nav>

    <div class="icons">
        <div class="fas fa-bars" id="menu-btn"></div>
        <div class="fas fa-search" id="search-btn"></div>
        <div class="fas fa-shopping-cart" id="cart-btn"></div>
        <div class="fas fa-user" id="login-btn"></div>
    </div>

    

</header>

<!-- header section ends -->

<!-- home section starts  -->

<section class="home" id="home">
    <div class="content">
        <h3>Back<span> To</span> Nature</h3>
        <p>Keselamatan nomor 1, outfit nomer 2.</p>
        <a href="#" class="btn">shop now</a>
    </div>

</section>

<!-- home section ends -->

<!-- features section starts  -->

<section class="features" id="features">

    <h1 class="heading"> our <span>features</span> </h1>

    <div class="box-container">
         
        <div class="box">
            <img src="image/feature-img-1.png" alt="">
            <h3>trusted</h3>
            <p>Barang tidak sesuai? Barang tidak sampai kerumah? Kami refund 100%!</p>
            <a href="#" class="btn">read more</a>
        </div>

        <div class="box">
            <img src="image/feature-img-2.png" alt="">
            <h3>free delivery</h3>
            <p>Tidak perlu khawatir, untuk ongkir kami yang bayar!</p>
            <a href="#" class="btn">read more</a>
        </div>

        <div class="box">
            <img src="image/feature-img-3.png" alt="">
            <h3>easy payments</h3>
            <p>Pembayaran mempunyai banyak opsi, jadi gausah bingung!</p>
            <a href="#" class="btn">read more</a>
        </div>

    </div>

</section>

<!-- features section ends -->

<!-- products section starts  -->

<section class="products" id="products">

    <h1 class="heading"> our <span>products</span> </h1>

    <div class="swiper product-slider">

        <div class="swiper-wrapper">

            <div class="swiper-slide box">
                <img src="image/sepatu-2.jpg" alt="">
                <h3>EIGER TARANTULA 2.0</h3>
                <div class="price"> Rp.749.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

            <div class="swiper-slide box">
                <img src="image/sepatu-3.jpg" alt="">
                <h3>EIGER CAYMAN LITE ORIGINAL</h3>
                <div class="price"> Rp.809.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

            <div class="swiper-slide box">
                <img src="image/sepatu-4.jpg" alt="">
                <h3>EIGER POLLOCK SHOES ORIGINAL</h3>
                <div class="price"> Rp.1.200.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

            <div class="swiper-slide box">
                <img src="image/sepatu-1.jpg" alt="">
                <h3>EIGER PHYTON HIGH CUT</h3>
                <div class="price"> Rp.899.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

        </div>

    </div>

    <div class="swiper product-slider">

        <div class="swiper-wrapper">

            <div class="swiper-slide box">
                <img src="image/ransel-3.jpg" alt="">
                <h3>CARRIER EIGER HELARCTOS 60L</h3>
                <div class="price"> Rp.825.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

            <div class="swiper-slide box">
                <img src="image/ransel-4.jpg" alt="">
                <h3>EIGER WONDERLUST 60L</h3>
                <div class="price"> Rp.1.679.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

            <div class="swiper-slide box">
                <img src="image/ransel-1.jpg" alt="">
                <h3>EIGER RHINOS 60L</h3>
                <div class="price"> Rp.999.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

            <div class="swiper-slide box">
                <img src="image/ransel-2.jpg" alt="">
                <h3>EIGER ELIPTIC SOLARIS 55L</h3>
                <div class="price"> Rp.1.249.000/- </div>
                <div class="stars">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                </div>
                <a href="#" class="btn">add to cart</a>
            </div>

        </div>

    </div>


</section>

<!-- products section ends -->

<!-- order section -->

<h1 class="heading"> to <span>order</span> </h1>

<form method="GET" action="pesanan_selesai.php">

    <div class="formulir">
    <label>Nama Lengkap</label>
    <input name="nama" type="text" required>

    <label>Alamat Pengiriman</label>
    <input name="alamat_pengiriman" type="text" required>

    <label>Nomor Telepon</label>
    <input name="no_telepon" type="text" required>

    <label>Jenis Pengiriman</label>
    <select name="jenis_pengiriman">
      <option value="Reguler">reguler</option>
      <option value="Paket Kilat">Paket Kilat</option>
      <option value="Kargo">Kargo</option>
    </select>

    <label>Jasa Pengiriman</label>
    <select name="kurir">
      <option value="JNE">JNE</option>
      <option value="J&T">J&T</option>
      <option value="Si Cepat">Si Cepat</option>
      <option value="ID Xpress">ID Express</option>
      <option value="TIKI">TIKI</option>
    </select>

    <label>Metode Pembayaran</label>
    <select name="metode_pembayaran">
      <option value="Transfer BANK">Transfer Bank</option>
      <option value="Indomaret">Indomaret</option>
      <option value="Alfamart">Alfamart</option>
      <option value="OVO">OVO</option>
      <option value="GOPAY">GOPAY</option>
      <option value="DANA">DANA</option>
      <option value="COD">Cash On Delivery / COD</option>
    </select>

    <label>Barang Yang Dipesan dan Jumlahnya</label>
    <input name="barang" type="text" placeholder="Contoh : EIGER RHINOS 60L (2), EIGER WONDERLUST 60L (1)">
    </div>
        <button>Pesan Sekarang</button>
 </form>

 <!-- order section ends -->


<!-- footer section starts  -->

<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3> shappy </h3>
            <p>Keselamatan nomer satu, outfit nomer 2.</p>
        </div>

        <div class="box">
            <h3>contact info</h3>
            <a href="#" class="links"> <i class="fas fa-phone"></i> +123-456-7890 </a>
            <a href="#" class="links"> <i class="fas fa-phone"></i> +081-291-2927 </a>
            <a href="#" class="links"> <i class="fas fa-envelope"></i> jananiadel76@gmail.com </a>
            <a href="#" class="links"> <i class="fas fa-map-marker-alt"></i> purwokerto, indonesia - 400104 </a>
        </div>

        <div class="box">
            <h3>quick links</h3>
            <a href="#" class="links"> <i class="fas fa-arrow-right"></i> home </a>
            <a href="#" class="links"> <i class="fas fa-arrow-right"></i> features </a>
            <a href="#" class="links"> <i class="fas fa-arrow-right"></i> products </a>
            <a href="#" class="links"> <i class="fas fa-arrow-right"></i> order </a>
        </div>

    </div>

    <div class="credit"> created by <span> mr. adel janani </span> | all rights reserved </div>

</section>

<!-- footer section ends -->















<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

<!-- custom js file link  -->
<script src="js/script.js"></script>

</body>
</html>